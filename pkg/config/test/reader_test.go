// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// chatgpt wrap - api tester for whisper and chatgpt
// Copyright (C) 2023 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package test

import (
	"os"
	"testing"

	config "gitlab.com/quoeamaster/chatgpt-wrap/pkg/config"
)

func Test_new(t *testing.T) {
	// create from env
	os.Setenv(config.KeyOpenApiKey, "some-secret-here")
	defer os.Unsetenv(config.KeyOpenApiKey)

	if o, e := config.NewFileReader(""); nil != e {
		t.Fatalf("[Test_new.env] failed to create the fileReader, %v", e)
	} else {
		if "some-secret-here" != o.Configs().Auth.Key {
			t.Fatalf("[Test_new.env] expect some-secret-here but... %v", o.Configs().Auth.Key)
		}
	}

	// create with a file
	if o, e := config.NewFileReader("./config.00.toml"); nil != e {
		t.Fatalf("[Test_new.file] failed to create the fileReader, %v", e)
	} else {
		if "some-secret-value" != o.Configs().Auth.Key {
			t.Fatalf("[Test_new.file] expect some-secret-value but... %v", o.Configs().Auth.Key)
		}
	}
}
