// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// chatgpt wrap - api tester for whisper and chatgpt
// Copyright (C) 2023 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package config

import (
	"fmt"
	"io/ioutil"
	"os"

	toml "github.com/pelletier/go-toml"
)

const (
	KeyOpenApiKey string = "OPEN_AI_API_KEY"
)

// FileEnvReader is a reader to get configurations through a config file or
// directly through Environment variables.
type FileEnvReader struct {
	// file is the location of the config file.
	file string

	// configs is the ORM of a config file.
	configs Configuration
}

// Configuration represents the toml config file in the programming way. (ORM)
type Configuration struct {
	Auth Auth
}

// Auth is part of the Configuration file's ORM.
type Auth struct {
	Key string
}

// NewFileReader creates an instance of the config reader based on the config file "file".
func NewFileReader(file string) (o *FileEnvReader, e error) {
	o = new(FileEnvReader)
	o.file = file

	// should a file be read or just env?
	if "" != o.file {
		b, e2 := ioutil.ReadFile(o.file)
		if nil != e {
			e = fmt.Errorf("[reader.new] failed to read config file, reason: %v", e2)
			return
		}
		o.configs = Configuration{}
		e = toml.Unmarshal(b, &o.configs)

		return
	}
	// env
	o.configs = Configuration{
		Auth: Auth{},
	}
	// TODO: setup everything from the env starting from here
	o.configs.Auth.Key = os.Getenv(KeyOpenApiKey)

	return
}

// Configs returns the configuration information read.
func (s *FileEnvReader) Configs() Configuration {
	return s.configs
}
