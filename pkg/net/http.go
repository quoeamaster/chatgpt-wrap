// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// chatgpt wrap - api tester for whisper and chatgpt
// Copyright (C) 2023 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package net

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"strings"
	"time"
)

// ServeHttp is a helper method to run http request.
func ServeHttp(method, url, payloadInString, token string) (result []byte, e error) {
	req, e := http.NewRequest(method, url, strings.NewReader(payloadInString))
	if nil != e {
		return
	}
	// headers
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", fmt.Sprintf("Bearer %v", token))

	// client creation
	d, _ := time.ParseDuration("2m")
	client := http.Client{
		Timeout: d,
	}
	res, e := client.Do(req)
	if nil != e {
		return
	}
	// result in bytes
	result, e = ioutil.ReadAll(res.Body)
	defer res.Body.Close()

	return
}

// serverHttps (certfile string)
