// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// chatgpt wrap - api tester for whisper and chatgpt
// Copyright (C) 2023 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package chatgpt

import (
	"encoding/json"
	"net/http"

	"gitlab.com/quoeamaster/chatgpt-wrap/pkg/net"
)

// clientV1 - v1's version.
type clientV1 struct {
	// endpoint is the chatgpt API endpoint to call.
	endpoint string
	// token is the auth token for chatgpt.
	token string
}

func (s *clientV1) ForAnswer(params APIParams) (result []byte, e error) {
	// build payload
	if "" == params.Model {
		params.Model = ModelDavinci
	}
	p := make(map[string]interface{})
	p["model"] = params.Model
	p["prompt"] = params.Prompt
	p["temperature"] = params.Temperature
	p["max_tokens"] = params.MaxTokens
	p["top_p"] = params.TopP
	p["frequency_penalty"] = params.FrequencyPenalty
	p["presence_penalty"] = params.PresencePenalty
	p["stop"] = params.Stop

	pInBytes, e := json.Marshal(p)
	if nil != e {
		return
	}
	// run
	result, e = net.ServeHttp(http.MethodPost, s.endpoint, string(pInBytes), s.token)

	return
}

func (s *clientV1) ForCode(params APIParams) (result []byte, e error) {
	// build payload
	if "" == params.Model {
		params.Model = ModelCodeDavinci
	}
	p := make(map[string]interface{})
	p["model"] = params.Model
	p["prompt"] = params.Prompt
	p["max_tokens"] = params.MaxTokens
	p["temperature"] = params.Temperature
	p["top_p"] = params.TopP
	p["frequency_penalty"] = params.FrequencyPenalty
	p["presence_penalty"] = params.PresencePenalty
	p["stop"] = params.Stop

	pInBytes, e := json.Marshal(p)
	if nil != e {
		return
	}
	// run
	result, e = net.ServeHttp(http.MethodPost, s.endpoint, string(pInBytes), s.token)

	return
}
