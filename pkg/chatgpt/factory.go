// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// chatgpt wrap - api tester for whisper and chatgpt
// Copyright (C) 2023 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package chatgpt

import (
	"fmt"
	"os"

	"gitlab.com/quoeamaster/chatgpt-wrap/pkg/config"
)

const (
	API_v1 string = "v1"

	EndpointV1       string = "https://api.openai.com/v1/completions"
	ModelDavinci     string = "text-davinci-003"
	ModelCodeDavinci string = "code-davinci-002"

	KeyConfigFile string = "CWRAP_CONFIG"
)

// GenerateClient creates a suitable client based on the provided "version". (e.g. v1)
func GenerateClient(version string) (o IClient, e error) {
	reader, e := config.NewFileReader(os.Getenv(KeyConfigFile))
	if nil != e {
		return
	}
	switch version {
	case API_v1:
		c := clientV1{
			endpoint: EndpointV1,
			token:    reader.Configs().Auth.Key,
		}
		o = &c
	default:
		e = fmt.Errorf("[factory.generateClient] failed to support '%v'", version)
	}
	return
}

// APIParams is a struct holding various arguments for method calls to chatgpt.
//
// This struct would cover all API versions, hence simply set the required arguments for your targeted chatgpt API to call.
type APIParams struct {
	Model            string
	Prompt           string
	Temperature      float32
	MaxTokens        int
	TopP             float32
	FrequencyPenalty float32
	PresencePenalty  float32
	Stop             []string
}

// IClient represents the chatgpt client of a specific version (e.g. v1) that developers would interact with.
type IClient interface {
	// ForAnswer handles the following challenges:
	// QnA, Factual answering, tutor
	ForAnswer(params APIParams) (result []byte, e error)

	// ForCode handles the following challenges:
	// Javascript chatbot,
	ForCode(params APIParams) (result []byte, e error)
}
