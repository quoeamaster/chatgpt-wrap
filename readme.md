
## ChatGPT-Wrap - a chatgpt api wrapper for Go

ChatGPT-Wrap is a wrapper for calling chatgpt api(s) in Go. 

### usage example - Question and Answer example

```go
package main

import (
	"fmt"

	"gitlab.com/quoeamaster/chatgpt-wrap/pkg/chatgpt"
)

func main() {
	// if an env var CWRAP_CONFIG is set, the client would chose to use "file" mode and read the value associated under this envar
	// else the client would chose to use "env" mode and tries to get all important config values from envar.

  // #1
	client, e := chatgpt.GenerateClient(chatgpt.API_v1)
	if nil != e {
		panic(fmt.Errorf("[main] failed to generate client, reason: %v", e))
	}
  // #2
	params := chatgpt.APIParams{
		Prompt:    "I am a highly intelligent question answering bot. If you ask me a question that is rooted in truth, I will give you the answer. If you ask me a question that is nonsense, trickery, or has no clear answer, I will respond with \"Unknown\".\n\nQ: What is human life expectancy in the United States?\nA: Human life expectancy in the United States is 78 years.\n\nQ: Who was president of the United States in 1955?\nA: Dwight D. Eisenhower was president of the United States in 1955.\n\nQ: Which party did he belong to?\nA: He belonged to the Republican Party.\n\nQ: What is the square root of banana?\nA: Unknown\n\nQ: How does a telescope work?\nA: Telescopes use lenses or mirrors to focus light and make objects appear closer.\n\nQ: Where were the 1992 Olympics held?\nA: The 1992 Olympics were held in Barcelona, Spain.\n\nQ: How many squigs are in a bonk?\nA: Unknown\n\nQ: Where is the Valley of Kings?\nA:",
		MaxTokens: 100,
		TopP:      1.0,
		Stop:      []string{"\n"},
	}
  // #3
	result, e := client.ForAnswer(params)
	if nil != e {
		panic(fmt.Errorf("[main] failed to run request, reason %v", e))
	}
  // #4
	fmt.Println(string(result))
}
```

steps:
  - #1 create the wrapper client based on version (at the moment of time, only version "__v1__" is supported) 
  - #2 prepare the api params for the api call
  - #3 call the __ForAnswer()__ api on the client to get the completion answer from chatgpt side; if you are looking for code answers call the __ForCode()__ instead
  - #4 the return payload are in __[]byte__ form which means you could transform it to back to a __struct__ or __map[string]interface{}__ through json unmarshalling to access the values

for for examples and details, please check the unit tests.

### startup configurations

Typically the most important configuration required is the __openai api key__ which could be provided by 2 ways:
  - a TOML config file (e.g. config.toml) which is specified by the Environment Variable "__CWRAP_CONFIG__" OR
  - setup an Environment Variable named "__OPEN_AI_API_KEY__" holding the api key's value

a sample of the config toml file as follows:
```toml
[auth]
key = "REPLACE-WITH-YOUR-OPENAI-API-KEY-HERE"
```

__PS__. the TOML config file approach would be recommended as future releases would favour more on configuration flexiblities.

### releases

**PS**. chatgpt is actively developing and hence the corresponding api might also change in the coming future and hence updated releases on the wrapper would be available accordingly.

| version   |      descriptions      |
|:----------:|:-------------|
| v1.0.0 | provides common __TEXT__ and __CODE__ chat completion apis based on _text-davinci-003_ and _code-davinci-002_ models under the __GPT-3.5__ family |



