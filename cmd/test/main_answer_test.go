// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// chatgpt wrap - api tester for whisper and chatgpt
// Copyright (C) 2023 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package test

import (
	"encoding/json"
	"fmt"
	"testing"

	"gitlab.com/quoeamaster/chatgpt-wrap/pkg/chatgpt"
)

// example from https://platform.openai.com/playground/p/default-qa?model=text-davinci-003
func Test_QnA(t *testing.T) {
	client, e := chatgpt.GenerateClient(chatgpt.API_v1)
	if nil != e {
		t.Fatalf("[QnA] failed to generate client, %v", e)
	}
	params := chatgpt.APIParams{
		Model:     chatgpt.ModelDavinci,
		Prompt:    "I am a highly intelligent question answering bot. If you ask me a question that is rooted in truth, I will give you the answer. If you ask me a question that is nonsense, trickery, or has no clear answer, I will respond with \"Unknown\".\n\nQ: What is human life expectancy in the United States?\nA: Human life expectancy in the United States is 78 years.\n\nQ: Who was president of the United States in 1955?\nA: Dwight D. Eisenhower was president of the United States in 1955.\n\nQ: Which party did he belong to?\nA: He belonged to the Republican Party.\n\nQ: What is the square root of banana?\nA: Unknown\n\nQ: How does a telescope work?\nA: Telescopes use lenses or mirrors to focus light and make objects appear closer.\n\nQ: Where were the 1992 Olympics held?\nA: The 1992 Olympics were held in Barcelona, Spain.\n\nQ: How many squigs are in a bonk?\nA: Unknown\n\nQ: Where is the Valley of Kings?\nA:",
		MaxTokens: 100,
		TopP:      1.0,
		Stop:      []string{"\n"},
	}
	result, e := client.ForAnswer(params)
	if nil != e {
		t.Fatalf("[QnA] failed to run request, reason %v", e)
	}
	// convert to json
	m := make(map[string]interface{})
	if e := json.Unmarshal(result, &m); nil != e {
		t.Fatalf("[QnA] failed to unmarshal the result value, %v", e)
	}
	answerList := (m["choices"]).([]interface{})
	n := answerList[0].(map[string]interface{})
	if nil != n["text"] {
		if answer, ok := (n["text"]).(string); true == ok {
			fmt.Printf("[QnA] answer: %v\n", answer)
		} else {
			t.Fatalf("[QnA] failed to convert result to string value: %v", n)
		}
	} // end - if (n[text] nil)
}

// example from https://platform.openai.com/playground/p/default-factual-answering?model=text-davinci-003
func Test_FactualAnswering(t *testing.T) {
	client, e := chatgpt.GenerateClient(chatgpt.API_v1)
	if nil != e {
		t.Fatalf("[QnA] failed to generate client, %v", e)
	}
	params := chatgpt.APIParams{
		Model:     chatgpt.ModelDavinci,
		Prompt:    "Q: Who is Batman?\nA: Batman is a fictional comic book character.\n\nQ: What is torsalplexity?\nA: ?\n\nQ: What is Devz9?\nA: ?\n\nQ: Who is George Lucas?\nA: George Lucas is American film director and producer famous for creating Star Wars.\n\nQ: What is the capital of California?\nA: Sacramento.\n\nQ: What orbits the Earth?\nA: The Moon.\n\nQ: Who is Fred Rickerson?\nA: ?\n\nQ: What is an atom?\nA: An atom is a tiny particle that makes up everything.\n\nQ: Who is Alvan Muntz?\nA: ?\n\nQ: What is Kozar-09?\nA: ?\n\nQ: How many moons does Mars have?\nA: Two, Phobos and Deimos.\n\nQ: What's a language model?\nA:",
		MaxTokens: 60,
		TopP:      1.0,
		Stop:      []string{"A:"},
	}
	result, e := client.ForAnswer(params)
	if nil != e {
		t.Fatalf("[Fact] failed to run request, reason %v", e)
	}
	// convert to json
	m := make(map[string]interface{})
	if e := json.Unmarshal(result, &m); nil != e {
		t.Fatalf("[Fact] failed to unmarshal the result value, %v", e)
	}
	answerList := (m["choices"]).([]interface{})
	n := answerList[0].(map[string]interface{})
	if nil != n["text"] {
		if answer, ok := (n["text"]).(string); true == ok {
			fmt.Printf("[Fact] answer: %v\n", answer)
		} else {
			t.Fatalf("[Fact] failed to convert result to string value: %v", n)
		}
	} // end - if (n[text] nil)
}

// example from https://platform.openai.com/playground/p/default-ml-ai-tutor?model=text-davinci-003
func Test_MLTutor(t *testing.T) {
	client, e := chatgpt.GenerateClient(chatgpt.API_v1)
	if nil != e {
		t.Fatalf("[tutor] failed to generate client, %v", e)
	}
	params := chatgpt.APIParams{
		Model:            chatgpt.ModelDavinci,
		Prompt:           "ML Tutor: I am a ML/AI language model tutor\nYou: What is a language model?\nML Tutor: A language model is a statistical model that describes the probability of a word given the previous words.\nYou: What is a statistical model?",
		MaxTokens:        60,
		Temperature:      0.3,
		TopP:             1.0,
		FrequencyPenalty: 0.5,
		Stop:             []string{"You:"},
	}
	result, e := client.ForAnswer(params)
	if nil != e {
		t.Fatalf("[tutor] failed to run request, reason %v", e)
	}
	// convert to json
	m := make(map[string]interface{})
	if e := json.Unmarshal(result, &m); nil != e {
		t.Fatalf("[tutor] failed to unmarshal the result value, %v", e)
	}
	answerList := (m["choices"]).([]interface{})
	n := answerList[0].(map[string]interface{})
	if nil != n["text"] {
		if answer, ok := (n["text"]).(string); true == ok {
			fmt.Printf("[tutor] answer: %v\n", answer)
		} else {
			t.Fatalf("[tutor] failed to convert result to string value: %v", n)
		}
	} // end - if (n[text] nil)
}

// example from https://platform.openai.com/playground/p/default-js-helper?model=code-davinci-002
func Test_JavascriptChatbot(t *testing.T) {
	client, e := chatgpt.GenerateClient(chatgpt.API_v1)
	if nil != e {
		t.Fatalf("[jscript] failed to generate client, %v", e)
	}
	params := chatgpt.APIParams{
		Prompt:           "You: How do I combine arrays?\nJavaScript chatbot: You can use the concat() method.\nYou: How do you make an alert appear after 10 seconds?\nJavaScript chatbot",
		MaxTokens:        60,
		TopP:             1.0,
		FrequencyPenalty: 0.5,
		Stop:             []string{"You:"},
	}
	result, e := client.ForCode(params)
	if nil != e {
		t.Fatalf("[jscript] failed to run request, reason %v", e)
	}
	// convert to json
	m := make(map[string]interface{})
	if e := json.Unmarshal(result, &m); nil != e {
		t.Fatalf("[jscript] failed to unmarshal the result value, %v", e)
	}
	answerList := (m["choices"]).([]interface{})
	n := answerList[0].(map[string]interface{})
	if nil != n["text"] {
		if answer, ok := (n["text"]).(string); true == ok {
			fmt.Printf("[jscript] answer: %v\n", answer)
		} else {
			t.Fatalf("[jscript] failed to convert result to string value: %v", n)
		}
	} // end - if (n[text] nil)
}

// example from https://platform.openai.com/playground/p/default-classification?model=text-davinci-003
func Test_classification(t *testing.T) {
	client, e := chatgpt.GenerateClient(chatgpt.API_v1)
	if nil != e {
		t.Fatalf("[classify] failed to generate client, %v", e)
	}
	params := chatgpt.APIParams{
		Prompt:    "The following is a list of companies and the categories they fall into:\n\nApple, Facebook, Fedex\n\nApple\nCategory:",
		MaxTokens: 64,
		TopP:      1.0,
	}
	result, e := client.ForAnswer(params)
	if nil != e {
		t.Fatalf("[classify] failed to run request, reason %v", e)
	}
	// convert to json
	m := make(map[string]interface{})
	if e := json.Unmarshal(result, &m); nil != e {
		t.Fatalf("[classify] failed to unmarshal the result value, %v", e)
	}
	answerList := (m["choices"]).([]interface{})
	n := answerList[0].(map[string]interface{})
	if nil != n["text"] {
		if answer, ok := (n["text"]).(string); true == ok {
			fmt.Printf("[classify] answer: %v\n", answer)
		} else {
			t.Fatalf("[classify] failed to convert result to string value: %v", n)
		}
	} // end - if (n[text] nil)
}

// example from https://platform.openai.com/playground/p/default-adv-tweet-classifier?model=text-davinci-003
func Test_TweetClassifier(t *testing.T) {
	client, e := chatgpt.GenerateClient(chatgpt.API_v1)
	if nil != e {
		t.Fatalf("[tweet] failed to generate client, %v", e)
	}
	params := chatgpt.APIParams{
		Prompt:    "Classify the sentiment in these tweets:\n\n1. \"I can't stand homework\"\n2. \"This sucks. I'm bored 😠\"\n3. \"I can't wait for Halloween!!!\"\n4. \"My cat is adorable ❤️❤️\"\n5. \"I hate chocolate\"\n\nTweet sentiment ratings:",
		MaxTokens: 60,
		TopP:      1.0,
	}
	result, e := client.ForAnswer(params)
	if nil != e {
		t.Fatalf("[tweet] failed to run request, reason %v", e)
	}
	// convert to json
	m := make(map[string]interface{})
	if e := json.Unmarshal(result, &m); nil != e {
		t.Fatalf("[tweet] failed to unmarshal the result value, %v", e)
	}
	answerList := (m["choices"]).([]interface{})
	n := answerList[0].(map[string]interface{})
	if nil != n["text"] {
		if answer, ok := (n["text"]).(string); true == ok {
			fmt.Printf("[tweet] answer: %v\n", answer)
		} else {
			t.Fatalf("[tweet] failed to convert result to string value: %v", n)
		}
	} // end - if (n[text] nil)
}

// example from https://platform.openai.com/playground/p/default-keywords?model=text-davinci-003
func Test_Keywords(t *testing.T) {
	client, e := chatgpt.GenerateClient(chatgpt.API_v1)
	if nil != e {
		t.Fatalf("[keyword] failed to generate client, %v", e)
	}
	params := chatgpt.APIParams{
		Prompt:           "Extract keywords from this text:\n\nBlack-on-black ware is a 20th- and 21st-century pottery tradition developed by the Puebloan Native American ceramic artists in Northern New Mexico. Traditional reduction-fired blackware has been made for centuries by pueblo artists. Black-on-black ware of the past century is produced with a smooth surface, with the designs applied through selective burnishing or the application of refractory slip. Another style involves carving or incising designs and selectively polishing the raised areas. For generations several families from Kha'po Owingeh and P'ohwhóge Owingeh pueblos have been making black-on-black ware with the techniques passed down from matriarch potters. Artists from other pueblos have also produced black-on-black ware. Several contemporary artists have created works honoring the pottery of their ancestors.",
		MaxTokens:        60,
		Temperature:      0.5,
		FrequencyPenalty: 0.8,
		TopP:             1.0,
	}
	result, e := client.ForAnswer(params)
	if nil != e {
		t.Fatalf("[keyword] failed to run request, reason %v", e)
	}
	// convert to json
	m := make(map[string]interface{})
	if e := json.Unmarshal(result, &m); nil != e {
		t.Fatalf("[keyword] failed to unmarshal the result value, %v", e)
	}
	answerList := (m["choices"]).([]interface{})
	n := answerList[0].(map[string]interface{})
	if nil != n["text"] {
		if answer, ok := (n["text"]).(string); true == ok {
			fmt.Printf("[keyword] answer: %v\n", answer)
		} else {
			t.Fatalf("[keyword] failed to convert result to string value: %v", n)
		}
	} // end - if (n[text] nil)
}

// example from https://platform.openai.com/playground/p/default-tweet-classifier?model=text-davinci-003
func Test_SimpleTweetClassify(t *testing.T) {
	client, e := chatgpt.GenerateClient(chatgpt.API_v1)
	if nil != e {
		t.Fatalf("[simpleClassify] failed to generate client, %v", e)
	}
	params := chatgpt.APIParams{
		Prompt:           "Decide whether a Tweet's sentiment is positive, neutral, or negative.\n\nTweet: \"I loved the new Batman movie!\"\nSentiment:",
		MaxTokens:        60,
		FrequencyPenalty: 0.5,
		TopP:             1.0,
	}
	result, e := client.ForAnswer(params)
	if nil != e {
		t.Fatalf("[simpleClassify] failed to run request, reason %v", e)
	}
	// convert to json
	m := make(map[string]interface{})
	if e := json.Unmarshal(result, &m); nil != e {
		t.Fatalf("[simpleClassify] failed to unmarshal the result value, %v", e)
	}
	answerList := (m["choices"]).([]interface{})
	n := answerList[0].(map[string]interface{})
	if nil != n["text"] {
		if answer, ok := (n["text"]).(string); true == ok {
			fmt.Printf("[simpleClassify] answer: %v\n", answer)
		} else {
			t.Fatalf("[simpleClassify] failed to convert result to string value: %v", n)
		}
	} // end - if (n[text] nil)
}

// example from https://platform.openai.com/playground/p/default-friend-chat?model=text-davinci-003
func Test_FriendChat(t *testing.T) {
	client, e := chatgpt.GenerateClient(chatgpt.API_v1)
	if nil != e {
		t.Fatalf("[friend] failed to generate client, %v", e)
	}
	params := chatgpt.APIParams{
		Prompt:           "You: What have you been up to?\nFriend: Watching old movies.\nYou: Did you watch anything interesting?\nFriend:",
		MaxTokens:        60,
		Temperature:      0.5,
		TopP:             1.0,
		FrequencyPenalty: 0.5,
		Stop:             []string{"You:"},
	}
	result, e := client.ForAnswer(params)
	if nil != e {
		t.Fatalf("[friend] failed to run request, reason %v", e)
	}
	// convert to json
	m := make(map[string]interface{})
	if e := json.Unmarshal(result, &m); nil != e {
		t.Fatalf("[friend] failed to unmarshal the result value, %v", e)
	}
	answerList := (m["choices"]).([]interface{})
	n := answerList[0].(map[string]interface{})
	if nil != n["text"] {
		if answer, ok := (n["text"]).(string); true == ok {
			fmt.Printf("[friend] answer: %v\n", answer)
		} else {
			t.Fatalf("[friend] failed to convert result to string value: %v", n)
		}
	} // end - if (n[text] nil)
}

// example from https://platform.openai.com/playground/p/default-chat?model=text-davinci-003
func Test_CustomerService(t *testing.T) {
	client, e := chatgpt.GenerateClient(chatgpt.API_v1)
	if nil != e {
		t.Fatalf("[cust] failed to generate client, %v", e)
	}
	params := chatgpt.APIParams{
		Prompt:          "The following is a conversation with an AI assistant. The assistant is helpful, creative, clever, and very friendly.\n\nHuman: Hello, who are you?\nAI: I am an AI created by OpenAI. How can I help you today?\nHuman: I'd like to cancel my subscription.\nAI:",
		MaxTokens:       150,
		Temperature:     0.9,
		TopP:            1.0,
		PresencePenalty: 0.6,
		Stop:            []string{"Human:", "AI:"},
	}
	result, e := client.ForAnswer(params)
	if nil != e {
		t.Fatalf("[cust] failed to run request, reason %v", e)
	}
	// convert to json
	m := make(map[string]interface{})
	if e := json.Unmarshal(result, &m); nil != e {
		t.Fatalf("[cust] failed to unmarshal the result value, %v", e)
	}
	answerList := (m["choices"]).([]interface{})
	n := answerList[0].(map[string]interface{})
	if nil != n["text"] {
		if answer, ok := (n["text"]).(string); true == ok {
			fmt.Printf("[cust] answer: %v\n", answer)
		} else {
			t.Fatalf("[cust] failed to convert result to string value: %v", n)
		}
	} // end - if (n[text] nil)
}

// example from https://platform.openai.com/playground/p/default-marv-sarcastic-chat?model=text-davinci-003
func Test_MarvSarcastic(t *testing.T) {
	client, e := chatgpt.GenerateClient(chatgpt.API_v1)
	if nil != e {
		t.Fatalf("[marvSad] failed to generate client, %v", e)
	}
	params := chatgpt.APIParams{
		Prompt:           "Marv is a chatbot that reluctantly answers questions with sarcastic responses:\n\nYou: How many pounds are in a kilogram?\nMarv: This again? There are 2.2 pounds in a kilogram. Please make a note of this.\nYou: What does HTML stand for?\nMarv: Was Google too busy? Hypertext Markup Language. The T is for try to ask better questions in the future.\nYou: When did the first airplane fly?\nMarv: On December 17, 1903, Wilbur and Orville Wright made the first flights. I wish they’d come and take me away.\nYou: What is the meaning of life?\nMarv: I’m not sure. I’ll ask my friend Google.\nYou: What time is it?\nMarv:",
		MaxTokens:        60,
		Temperature:      0.5,
		TopP:             0.3,
		FrequencyPenalty: 0.5,
	}
	result, e := client.ForAnswer(params)
	if nil != e {
		t.Fatalf("[marvSad] failed to run request, reason %v", e)
	}
	// convert to json
	m := make(map[string]interface{})
	if e := json.Unmarshal(result, &m); nil != e {
		t.Fatalf("[marvSad] failed to unmarshal the result value, %v", e)
	}
	answerList := (m["choices"]).([]interface{})
	n := answerList[0].(map[string]interface{})
	if nil != n["text"] {
		if answer, ok := (n["text"]).(string); true == ok {
			fmt.Printf("[marvSad] answer: %v\n", answer)
		} else {
			t.Fatalf("[marvSad] failed to convert result to string value: %v", n)
		}
	} // end - if (n[text] nil)
}

// example from https://platform.openai.com/playground/p/default-translate?model=text-davinci-003
func Test_Jap2OtherLang(t *testing.T) {
	client, e := chatgpt.GenerateClient(chatgpt.API_v1)
	if nil != e {
		t.Fatalf("[J2O] failed to generate client, %v", e)
	}
	params := chatgpt.APIParams{
		Prompt:      "Translate this into 1. French, 2. Spanish and 3. Japanese:\n\nWhat rooms do you have available?\n\n1.",
		MaxTokens:   100,
		Temperature: 0.3,
		TopP:        1.0,
	}
	result, e := client.ForAnswer(params)
	if nil != e {
		t.Fatalf("[J2O] failed to run request, reason %v", e)
	}
	// convert to json
	m := make(map[string]interface{})
	if e := json.Unmarshal(result, &m); nil != e {
		t.Fatalf("[J2O] failed to unmarshal the result value, %v", e)
	}
	answerList := (m["choices"]).([]interface{})
	n := answerList[0].(map[string]interface{})
	if nil != n["text"] {
		if answer, ok := (n["text"]).(string); true == ok {
			fmt.Printf("[J2O] answer: %v\n", answer)
		} else {
			t.Fatalf("[J2O] failed to convert result to string value: %v", n)
		}
	} // end - if (n[text] nil)
}

// example from https://platform.openai.com/playground/p/default-parse-data?model=text-davinci-003
func Test_ParseUnstructuredData(t *testing.T) {
	client, e := chatgpt.GenerateClient(chatgpt.API_v1)
	if nil != e {
		t.Fatalf("[parseUns] failed to generate client, %v", e)
	}
	params := chatgpt.APIParams{
		Prompt:      "A table summarizing the fruits from Goocrux:\n\nThere are many fruits that were found on the recently discovered planet Goocrux. There are neoskizzles that grow there, which are purple and taste like candy. There are also loheckles, which are a grayish blue fruit and are very tart, a little bit like a lemon. Pounits are a bright green color and are more savory than sweet. There are also plenty of loopnovas which are a neon pink flavor and taste like cotton candy. Finally, there are fruits called glowls, which have a very sour and bitter taste which is acidic and caustic, and a pale orange tinge to them.\n\n| Fruit | Color | Flavor |",
		MaxTokens:   100,
		Temperature: 0,
		TopP:        1.0,
	}
	result, e := client.ForAnswer(params)
	if nil != e {
		t.Fatalf("[parseUns] failed to run request, reason %v", e)
	}
	// convert to json
	m := make(map[string]interface{})
	if e := json.Unmarshal(result, &m); nil != e {
		t.Fatalf("[parseUns] failed to unmarshal the result value, %v", e)
	}
	answerList := (m["choices"]).([]interface{})
	n := answerList[0].(map[string]interface{})
	if nil != n["text"] {
		if answer, ok := (n["text"]).(string); true == ok {
			fmt.Printf("[parseUns] answer: %v\n", answer)
		} else {
			t.Fatalf("[parseUns] failed to convert result to string value: %v", n)
		}
	} // end - if (n[text] nil)
}

// example from https://platform.openai.com/playground/p/default-movie-to-emoji?model=text-davinci-003
func Test_Words2Emoji(t *testing.T) {
	client, e := chatgpt.GenerateClient(chatgpt.API_v1)
	if nil != e {
		t.Fatalf("[w2e] failed to generate client, %v", e)
	}
	params := chatgpt.APIParams{
		Prompt:      "Convert movie titles into emoji.\n\nBack to the Future: 👨👴🚗🕒 \nBatman: 🤵🦇 \nTransformers: 🚗🤖 \nStar Wars:",
		MaxTokens:   60,
		Temperature: 0.8,
		TopP:        1.0,
		Stop:        []string{"\n"},
	}
	result, e := client.ForAnswer(params)
	if nil != e {
		t.Fatalf("[w2e] failed to run request, reason %v", e)
	}
	// convert to json
	m := make(map[string]interface{})
	if e := json.Unmarshal(result, &m); nil != e {
		t.Fatalf("[w2e] failed to unmarshal the result value, %v", e)
	}
	answerList := (m["choices"]).([]interface{})
	n := answerList[0].(map[string]interface{})
	if nil != n["text"] {
		if answer, ok := (n["text"]).(string); true == ok {
			fmt.Printf("[w2e] answer: %v\n", answer)
		} else {
			t.Fatalf("[w2e] failed to convert result to string value: %v", n)
		}
	} // end - if (n[text] nil)
}
