// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// chatgpt wrap - api tester for whisper and chatgpt
// Copyright (C) 2023 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package test

import (
	"encoding/json"
	"fmt"
	"testing"

	"gitlab.com/quoeamaster/chatgpt-wrap/pkg/chatgpt"
)

// example from https://platform.openai.com/playground/p/default-translate-code?model=code-davinci-002
func Test_TranslateProgram(t *testing.T) {
	client, e := chatgpt.GenerateClient(chatgpt.API_v1)
	if nil != e {
		t.Fatalf("[Test_TranslateProgram] failed to generate client, %v", e)
	}
	params := chatgpt.APIParams{
		Prompt:    "##### Translate this function  from Python into Haskell\n### Python\n    \n    def predict_proba(X: Iterable[str]):\n        return np.array([predict_one_probas(tweet) for tweet in X])\n    \n### Haskell",
		MaxTokens: 54,
		TopP:      1.0,
		Stop:      []string{"###"},
	}
	result, e := client.ForCode(params)
	if nil != e {
		t.Fatalf("[Test_TranslateProgram] failed to run request, reason %v", e)
	}
	// convert to json
	m := make(map[string]interface{})
	if e := json.Unmarshal(result, &m); nil != e {
		t.Fatalf("[Test_TranslateProgram] failed to unmarshal the result value, %v", e)
	}
	answerList := (m["choices"]).([]interface{})
	n := answerList[0].(map[string]interface{})
	if nil != n["text"] {
		if answer, ok := (n["text"]).(string); true == ok {
			fmt.Printf("[Test_TranslateProgram] answer: %v\n", answer)
		} else {
			t.Fatalf("[Test_TranslateProgram] failed to convert result to string value: %v", n)
		}
	} // end - if (n[text] nil)
}

// example from https://platform.openai.com/playground/p/default-explain-code?model=code-davinci-002
func Test_ExplainCode(t *testing.T) {
	client, e := chatgpt.GenerateClient(chatgpt.API_v1)
	if nil != e {
		t.Fatalf("[explain] failed to generate client, %v", e)
	}
	params := chatgpt.APIParams{
		Prompt:    "class Log:\n    def __init__(self, path):\n        dirname = os.path.dirname(path)\n        os.makedirs(dirname, exist_ok=True)\n        f = open(path, \"a+\")\n\n        # Check that the file is newline-terminated\n        size = os.path.getsize(path)\n        if size > 0:\n            f.seek(size - 1)\n            end = f.read(1)\n            if end != \"\\n\":\n                f.write(\"\\n\")\n        self.f = f\n        self.path = path\n\n    def log(self, event):\n        event[\"_event_id\"] = str(uuid.uuid4())\n        json.dump(event, self.f)\n        self.f.write(\"\\n\")\n\n    def state(self):\n        state = {\"complete\": set(), \"last\": None}\n        for line in open(self.path):\n            event = json.loads(line)\n            if event[\"type\"] == \"submit\" and event[\"success\"]:\n                state[\"complete\"].add(event[\"id\"])\n                state[\"last\"] = event\n        return state\n\n\"\"\"\nHere's what the above class is doing:\n1.",
		MaxTokens: 64,
		TopP:      1.0,
		Stop:      []string{"\"\"\""},
	}
	result, e := client.ForCode(params)
	if nil != e {
		t.Fatalf("[explain] failed to run request, reason %v", e)
	}
	// convert to json
	m := make(map[string]interface{})
	if e := json.Unmarshal(result, &m); nil != e {
		t.Fatalf("[explain] failed to unmarshal the result value, %v", e)
	}
	answerList := (m["choices"]).([]interface{})
	n := answerList[0].(map[string]interface{})
	if nil != n["text"] {
		if answer, ok := (n["text"]).(string); true == ok {
			fmt.Printf("[explain] answer: %v\n", answer)
		} else {
			t.Fatalf("[explain] failed to convert result to string value: %v", n)
		}
	} // end - if (n[text] nil)
}

// example from https://platform.openai.com/playground/p/default-fix-python-bugs?model=code-davinci-002
func Test_PythonBug(t *testing.T) {
	client, e := chatgpt.GenerateClient(chatgpt.API_v1)
	if nil != e {
		t.Fatalf("[bug] failed to generate client, %v", e)
	}
	params := chatgpt.APIParams{
		Prompt:    "##### Fix bugs in the below function\n \n### Buggy Python\nimport Random\na = random.randint(1,12)\nb = random.randint(1,12)\nfor i in range(10):\n    question = \"What is \"+a+\" x \"+b+\"? \"\n    answer = input(question)\n    if answer = a*b\n        print (Well done!)\n    else:\n        print(\"No.\")\n    \n### Fixed Python",
		MaxTokens: 182,
		TopP:      1.0,
		Stop:      []string{"###"},
	}
	result, e := client.ForCode(params)
	if nil != e {
		t.Fatalf("[bug] failed to run request, reason %v", e)
	}
	// convert to json
	m := make(map[string]interface{})
	if e := json.Unmarshal(result, &m); nil != e {
		t.Fatalf("[bug] failed to unmarshal the result value, %v", e)
	}
	answerList := (m["choices"]).([]interface{})
	n := answerList[0].(map[string]interface{})
	if nil != n["text"] {
		if answer, ok := (n["text"]).(string); true == ok {
			fmt.Printf("[bug] answer: %v\n", answer)
		} else {
			t.Fatalf("[bug] failed to convert result to string value: %v", n)
		}
	} // end - if (n[text] nil)
}

// example from https://platform.openai.com/playground/p/default-js-to-py?model=code-davinci-002
func Test_TranslateJscript2Python(t *testing.T) {
	client, e := chatgpt.GenerateClient(chatgpt.API_v1)
	if nil != e {
		t.Fatalf("[J2P] failed to generate client, %v", e)
	}
	params := chatgpt.APIParams{
		Prompt:    "#JavaScript to Python:\nJavaScript: \ndogs = [\"bill\", \"joe\", \"carl\"]\ncar = []\ndogs.forEach((dog) {\n    car.push(dog);\n});\n\nPython:",
		MaxTokens: 182,
		TopP:      1.0,
	}
	result, e := client.ForCode(params)
	if nil != e {
		t.Fatalf("[J2P] failed to run request, reason %v", e)
	}
	// convert to json
	m := make(map[string]interface{})
	if e := json.Unmarshal(result, &m); nil != e {
		t.Fatalf("[J2P] failed to unmarshal the result value, %v", e)
	}
	answerList := (m["choices"]).([]interface{})
	n := answerList[0].(map[string]interface{})
	if nil != n["text"] {
		if answer, ok := (n["text"]).(string); true == ok {
			fmt.Printf("[J2P] answer: %v\n", answer)
		} else {
			t.Fatalf("[J2P] failed to convert result to string value: %v", n)
		}
	} // end - if (n[text] nil)
}

// example from https://platform.openai.com/playground/p/default-python-docstring?model=code-davinci-002
func Test_PythoDocstring(t *testing.T) {
	client, e := chatgpt.GenerateClient(chatgpt.API_v1)
	if nil != e {
		t.Fatalf("[doc] failed to generate client, %v", e)
	}
	params := chatgpt.APIParams{
		Prompt:    "# Python 3.7\n \ndef randomly_split_dataset(folder, filename, split_ratio=[0.8, 0.2]):\n    df = pd.read_json(folder + filename, lines=True)\n    train_name, test_name = \"train.jsonl\", \"test.jsonl\"\n    df_train, df_test = train_test_split(df, test_size=split_ratio[1], random_state=42)\n    df_train.to_json(folder + train_name, orient='records', lines=True)\n    df_test.to_json(folder + test_name, orient='records', lines=True)\nrandomly_split_dataset('finetune_data/', 'dataset.jsonl')\n    \n# An elaborate, high quality docstring for the above function:\n\"\"\"",
		MaxTokens: 150,
		TopP:      1.0,
		Stop:      []string{"#", "\"\"\""},
	}
	result, e := client.ForCode(params)
	if nil != e {
		t.Fatalf("[doc] failed to run request, reason %v", e)
	}
	// convert to json
	m := make(map[string]interface{})
	if e := json.Unmarshal(result, &m); nil != e {
		t.Fatalf("[doc] failed to unmarshal the result value, %v", e)
	}
	answerList := (m["choices"]).([]interface{})
	n := answerList[0].(map[string]interface{})
	if nil != n["text"] {
		if answer, ok := (n["text"]).(string); true == ok {
			fmt.Printf("[doc] answer: %v\n", answer)
		} else {
			t.Fatalf("[doc] failed to convert result to string value: %v", n)
		}
	} // end - if (n[text] nil)
}

// example from https://platform.openai.com/playground/p/default-js-one-line?model=code-davinci-002
func Test_OneLineJscript(t *testing.T) {
	client, e := chatgpt.GenerateClient(chatgpt.API_v1)
	if nil != e {
		t.Fatalf("[1LJ] failed to generate client, %v", e)
	}
	params := chatgpt.APIParams{
		Prompt:    "Use list comprehension to convert this into one line of JavaScript:\n\ndogs.forEach((dog) => {\n    car.push(dog);\n});\n\nJavaScript one line version:",
		MaxTokens: 60,
		TopP:      1.0,
		Stop:      []string{";"},
	}
	result, e := client.ForCode(params)
	if nil != e {
		t.Fatalf("[1LJ] failed to run request, reason %v", e)
	}
	// convert to json
	m := make(map[string]interface{})
	if e := json.Unmarshal(result, &m); nil != e {
		t.Fatalf("[1LJ] failed to unmarshal the result value, %v", e)
	}
	answerList := (m["choices"]).([]interface{})
	n := answerList[0].(map[string]interface{})
	if nil != n["text"] {
		if answer, ok := (n["text"]).(string); true == ok {
			fmt.Printf("[1LJ] answer: %v\n", answer)
		} else {
			t.Fatalf("[1LJ] failed to convert result to string value: %v", n)
		}
	} // end - if (n[text] nil)
}

// example from https://platform.openai.com/playground/p/default-openai-api?model=code-davinci-002
func Test_NL2OpenAI(t *testing.T) {
	client, e := chatgpt.GenerateClient(chatgpt.API_v1)
	if nil != e {
		t.Fatalf("[NL2] failed to generate client, %v", e)
	}
	params := chatgpt.APIParams{
		Prompt:    "\"\"\"\nUtil exposes the following:\nutil.openai() -> authenticates & returns the openai module, which has the following functions:\nopenai.Completion.create(\n    prompt=\"<my prompt>\", # The prompt to start completing from\n    max_tokens=123, # The max number of tokens to generate\n    temperature=1.0 # A measure of randomness\n    echo=True, # Whether to return the prompt in addition to the generated completion\n)\n\"\"\"\nimport util\n\"\"\"\nCreate an OpenAI completion starting from the prompt \"Once upon an AI\", no more than 5 tokens. Does not include the prompt.\n\"\"\"\n",
		MaxTokens: 60,
		TopP:      1.0,
		Stop:      []string{"\"\"\""},
	}
	result, e := client.ForCode(params)
	if nil != e {
		t.Fatalf("[NL2] failed to run request, reason %v", e)
	}
	// convert to json
	m := make(map[string]interface{})
	if e := json.Unmarshal(result, &m); nil != e {
		t.Fatalf("[NL2] failed to unmarshal the result value, %v", e)
	}
	answerList := (m["choices"]).([]interface{})
	n := answerList[0].(map[string]interface{})
	if nil != n["text"] {
		if answer, ok := (n["text"]).(string); true == ok {
			fmt.Printf("[NL2@] answer: %v\n", answer)
		} else {
			t.Fatalf("[NL2] failed to convert result to string value: %v", n)
		}
	} // end - if (n[text] nil)
}

// example from https://platform.openai.com/playground/p/default-stripe-api?model=code-davinci-002
func Test_NL2Stripe(t *testing.T) {
	client, e := chatgpt.GenerateClient(chatgpt.API_v1)
	if nil != e {
		t.Fatalf("[stripe] failed to generate client, %v", e)
	}
	params := chatgpt.APIParams{
		Prompt:    "\"\"\"\nUtil exposes the following:\n\nutil.stripe() -> authenticates & returns the stripe module; usable as stripe.Charge.create etc\n\"\"\"\nimport util\n\"\"\"\nCreate a Stripe token using the users credit card: 5555-4444-3333-2222, expiration date 12 / 28, cvc 521\n\"\"\"",
		MaxTokens: 100,
		TopP:      1.0,
		Stop:      []string{"\"\"\""},
	}
	result, e := client.ForCode(params)
	if nil != e {
		t.Fatalf("[stripe] failed to run request, reason %v", e)
	}
	// convert to json
	m := make(map[string]interface{})
	if e := json.Unmarshal(result, &m); nil != e {
		t.Fatalf("[stripe] failed to unmarshal the result value, %v", e)
	}
	answerList := (m["choices"]).([]interface{})
	n := answerList[0].(map[string]interface{})
	if nil != n["text"] {
		if answer, ok := (n["text"]).(string); true == ok {
			fmt.Printf("[stripe] answer: %v\n", answer)
		} else {
			t.Fatalf("[stripe] failed to convert result to string value: %v", n)
		}
	} // end - if (n[text] nil)
}

// example from https://platform.openai.com/playground/p/default-sql-translate?model=code-davinci-002
func Test_SQL(t *testing.T) {
	client, e := chatgpt.GenerateClient(chatgpt.API_v1)
	if nil != e {
		t.Fatalf("[sql] failed to generate client, %v", e)
	}
	params := chatgpt.APIParams{
		Prompt:    "### Postgres SQL tables, with their properties:\n#\n# Employee(id, name, department_id)\n# Department(id, name, address)\n# Salary_Payments(id, employee_id, amount, date)\n#\n### A query to list the names of the departments which employed more than 10 employees in the last 3 months\nSELECT",
		MaxTokens: 150,
		TopP:      1.0,
		Stop:      []string{"#", ";"},
	}
	result, e := client.ForCode(params)
	if nil != e {
		t.Fatalf("[sql] failed to run request, reason %v", e)
	}
	// convert to json
	m := make(map[string]interface{})
	if e := json.Unmarshal(result, &m); nil != e {
		t.Fatalf("[sql] failed to unmarshal the result value, %v", e)
	}
	answerList := (m["choices"]).([]interface{})
	n := answerList[0].(map[string]interface{})
	if nil != n["text"] {
		if answer, ok := (n["text"]).(string); true == ok {
			fmt.Printf("[sql] answer: %v\n", answer)
		} else {
			t.Fatalf("[sql] failed to convert result to string value: %v", n)
		}
	} // end - if (n[text] nil)
}

// example from https://platform.openai.com/playground/p/default-python-to-natural-language?model=code-davinci-002
func Test_Python2NL(t *testing.T) {
	client, e := chatgpt.GenerateClient(chatgpt.API_v1)
	if nil != e {
		t.Fatalf("[p2n] failed to generate client, %v", e)
	}
	params := chatgpt.APIParams{
		Prompt:    "# Python 3 \ndef remove_common_prefix(x, prefix, ws_prefix): \n    x[\"completion\"] = x[\"completion\"].str[len(prefix) :] \n    if ws_prefix: \n        # keep the single whitespace as prefix \n        x[\"completion\"] = \" \" + x[\"completion\"] \nreturn x \n\n# Explanation of what the code does\n\n#",
		MaxTokens: 64,
		TopP:      1.0,
	}
	result, e := client.ForCode(params)
	if nil != e {
		t.Fatalf("[p2n] failed to run request, reason %v", e)
	}
	// convert to json
	m := make(map[string]interface{})
	if e := json.Unmarshal(result, &m); nil != e {
		t.Fatalf("[p2n] failed to unmarshal the result value, %v", e)
	}
	answerList := (m["choices"]).([]interface{})
	n := answerList[0].(map[string]interface{})
	if nil != n["text"] {
		if answer, ok := (n["text"]).(string); true == ok {
			fmt.Printf("[p2n] answer: %v\n", answer)
		} else {
			t.Fatalf("[p2n] failed to convert result to string value: %v", n)
		}
	} // end - if (n[text] nil)
}

// example from https://platform.openai.com/playground/p/default-time-complexity?model=text-davinci-003
func Test_TimeComplexity(t *testing.T) {
	client, e := chatgpt.GenerateClient(chatgpt.API_v1)
	if nil != e {
		t.Fatalf("[time] failed to generate client, %v", e)
	}
	params := chatgpt.APIParams{
		Prompt:    "def foo(n, k):\naccum = 0\nfor i in range(n):\n    for l in range(k):\n        accum += i\nreturn accum\n\"\"\"\nThe time complexity of this function is",
		MaxTokens: 64,
		TopP:      1.0,
		Stop:      []string{"\n"},
	}
	result, e := client.ForAnswer(params)
	if nil != e {
		t.Fatalf("[time] failed to run request, reason %v", e)
	}
	// convert to json
	m := make(map[string]interface{})
	if e := json.Unmarshal(result, &m); nil != e {
		t.Fatalf("[time] failed to unmarshal the result value, %v", e)
	}
	answerList := (m["choices"]).([]interface{})
	n := answerList[0].(map[string]interface{})
	if nil != n["text"] {
		if answer, ok := (n["text"]).(string); true == ok {
			fmt.Printf("[time] answer: %v\n", answer)
		} else {
			t.Fatalf("[time] failed to convert result to string value: %v", n)
		}
	} // end - if (n[text] nil)
}
