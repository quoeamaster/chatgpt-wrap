// GNU Affero General Public License V.3.0 or AGPL-3.0
//
// chatgpt wrap - api tester for whisper and chatgpt
// Copyright (C) 2023 - quoeamaster@gmail.com
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU Affero General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Affero General Public License for more details.
//
// You should have received a copy of the GNU Affero General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

package main

import (
	"fmt"

	"gitlab.com/quoeamaster/chatgpt-wrap/pkg/chatgpt"
)

func main() {
	// if an env var CWRAP_CONFIG is set, the client would chose to use "file" mode and read the value associated under this envar
	// else the client would chose to use "env" mode and tries to get all important config values from envar.
	client, e := chatgpt.GenerateClient(chatgpt.API_v1)
	if nil != e {
		panic(fmt.Errorf("[main] failed to generate client, reason: %v", e))
	}
	params := chatgpt.APIParams{
		Model:     chatgpt.ModelDavinci,
		Prompt:    "I am a highly intelligent question answering bot. If you ask me a question that is rooted in truth, I will give you the answer. If you ask me a question that is nonsense, trickery, or has no clear answer, I will respond with \"Unknown\".\n\nQ: What is human life expectancy in the United States?\nA: Human life expectancy in the United States is 78 years.\n\nQ: Who was president of the United States in 1955?\nA: Dwight D. Eisenhower was president of the United States in 1955.\n\nQ: Which party did he belong to?\nA: He belonged to the Republican Party.\n\nQ: What is the square root of banana?\nA: Unknown\n\nQ: How does a telescope work?\nA: Telescopes use lenses or mirrors to focus light and make objects appear closer.\n\nQ: Where were the 1992 Olympics held?\nA: The 1992 Olympics were held in Barcelona, Spain.\n\nQ: How many squigs are in a bonk?\nA: Unknown\n\nQ: Where is the Valley of Kings?\nA:",
		MaxTokens: 100,
		TopP:      1.0,
		Stop:      []string{"\n"},
	}
	result, e := client.ForAnswer(params)
	if nil != e {
		panic(fmt.Errorf("[main] failed to run request, reason %v", e))
	}
	fmt.Println(string(result))
}
